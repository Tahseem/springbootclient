package com.atandt.springbootclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootclientApplication.class, args);
	}

}
